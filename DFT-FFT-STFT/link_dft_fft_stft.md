# Youtube links

- [Discrete Fourier Transform - Simple Step by Step](https://www.youtube.com/watch?v=mkGsMWi_j4Q&t=11s&ab_channel=SimonXu)
- [The Short Time Fourier Transform | Digital Signal Processing](https://www.youtube.com/watch?v=g1_wcbGUcDY&ab_channel=FreeEngineeringLectures)
- [Short-Time Fourier Transform Explained Easily](https://www.youtube.com/watch?v=-Yxj3yfvY-4&ab_channel=ValerioVelardo-TheSoundofAI)
- [The Fast Fourier Transform (FFT): Most Ingenious Algorithm Ever?](https://www.youtube.com/watch?v=h7apO7q16V0&t=409s&ab_channel=Reducible)
