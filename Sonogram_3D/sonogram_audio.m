clear all;
#   Read audio file
filename="ready.wav";
[x, fs] = audioread(file_in_loadpath(filename)); # audio file
#set sampleRate if you want a different sampling Frequency from audiofile sample Rate
fS=10000;
#set number of samples for Stft , HopSize and Type of window
nSamples = 512;
hopsize = nSamples/2;
window = blackmanharris(nSamples);

#remove comment-hash to have spectrogram 
#specgram(x, nSamples,fS, window, nSamples-hopsize);

# s for Magnitude, f for frequency bins, t for times
[s,f,t] = specgram(x, nSamples,fS, window, nSamples-hopsize);


function waterplot(s,f,t)
% Waterfall plot of spectrogram
    waterfall(f,t,abs(s)'.^2);
    #set(gca,XDir="reverse",View=[30 50]);
    #xlabel("Frequency (Hz)");
    #ylabel("Time (s)");
endfunction

waterplot(s,f,t);




