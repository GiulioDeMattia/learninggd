# Statistical Sonogram

## Requirements
Octave version 8.4.0

[package control](https://github.com/gnu-octave/pkg-control) 

[package signal](https://sourceforge.net/p/octave/signal/ci/default/tree)


## 3D Sonogram

Simple program in octave to realise waterfall plot of an audio signal (mono for now!).

![waterplot.gif](waterplot.gif)

### MATLAB LINKS
- [3D sonogram example](https://it.mathworks.com/help/signal/ref/spectrogram.html#d126e200270)


### OCTAVE LINKS
- [`specgram`](https://octave.sourceforge.io/signal/function/specgram.html)

### FFMPEG LINKS
- [Trim Silsence](https://superuser.com/questions/1362176/how-to-trim-silence-only-from-beginning-and-end-of-mp3-files-using-ffmpeg
)
- [AudioChannelManipulation](https://trac.ffmpeg.org/wiki/AudioChannelManipulation)

## TO DO LIST FOR STATISTICAL SONOGRAM IN OCTAVE

- [ ] Batch process audio files 
  - [ ] Bash script
    - [ ] Normalization
    - [ ] Silence Manipulation
    - [ ] Multiple channels management
- [ ] Export 3D sonogram as image within the octave script
- [ ] Retrieve data from `specgram` for statistical purposes.
- [ ] Show statistical sonogram data

## TO DO LIST FOR REAL TIME STATISTICAL SONOGRAM 

- [ ] Use OpenGL to use the GPU graphical acceleration for *real-time* purposes.


