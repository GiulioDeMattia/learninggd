fs = 600;
ts = 0:1/fs:2;
x = chirp(ts,250,ts(end),50,"quadratic",0,"convex","complex");
M = 20;
L = 1;
g = blackmanharris(M);
Ndft = 1024;

[s,f,t] = spectrogram(x,g,L,Ndft,fs);

waterplot(s,f,t);

function waterplot(s,f,t)
% Waterfall plot of spectrogram
    waterfall(f,t,abs(s)'.^2)
    set(gca,XDir="reverse",View=[30 50])
    xlabel("Frequency (Hz)")
    ylabel("Time (s)")
end
